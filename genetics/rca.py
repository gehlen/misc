#!/usr/bin/env python
"""Perform reciprocal cintioal analysis with plink2"""
import subprocess
import pandas as pd
import os
import click


def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""

    # from whichcraft import which
    from shutil import which

    return which(name) is not None


def comma_seperated(string):
    return [i.strip() for i in string.split(",")]


@click.command(help="Perform GWAS conditional analysis with Plink2")
@click.option("--snps", help="SNPs to test, seperated by comma", required=True)
@click.option(
    "--condition", help="SNPs to condition on, seperated by comma", required=True
)
@click.option("--pfile", required=True)
@click.option("--pheno", required=True)
@click.option("--pheno_name", required=True)
@click.option("--covar_name", required=True)
@click.option("--out", required=True)
def rca(snps, condition, pfile, pheno, pheno_name, covar_name, out):
    """Reciprocal conditional analysis with plink2"""
    if not is_tool("plink2"):
        raise OSError(
            "Plink2 not available. Please install: https://www.cog-genomics.org/plink/2.0/"
        )

    data = {}

    for c in comma_seperated(condition):
        print(f"Condition on {c}")
        # remove condtional snp from snps tested
        snp = ",".join(comma_seperated(snps))

        command = [
            "plink2",
            "--pfile",
            pfile,
            "--pheno",
            pheno,
            "--pheno-name",
            pheno_name,
            "--covar-name",
            covar_name,
            "--covar-variance-standardize",
            "--out",
            f"{out}.{c}",
            "--glm",
            "no-x-sex",
            "firth",
            "cols=chrom,pos,ref,alt,ax,firth,orbeta,se,ci,tz,p,err",
            "hide-covar",
            "--ci",
            "0.05",
            "--condition",
            c,
            "--snps",
            snp,
        ]
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = proc.communicate()
        if proc.returncode == 0:
            x = pd.read_csv(f"{out}.{c}.{pheno_name}.glm.firth", sep="\t")
            if len(x) > 0:
                data[c] = x
            if os.path.exists(f"{out}.{c}.{pheno_name}.glm.firth"):
                ...
                # os.remove(f"{out}.{pheno_name}.glm.firth")
                # os.remove(f"{out}.log")
        else:
            message = (
                "Shell command returned non-zero exit status: {0}\n\n"
                "Command:\n{1}\n\n"
                "Standard error:\n{2}"
            )
            raise IOError(message.format(proc.returncode, command, error.decode()))

    data = (
        pd.concat(data, names=["CONDITION", "DROP"])
        .reset_index("DROP", drop=True)
        .reset_index("CONDITION")
    )
    data = data.iloc[:, [*range(1, 4), 0, *range(4, data.shape[1])]]

    data.to_csv(f"{out}.rca.txt", sep="\t", index=False, header=True)


if __name__ == "__main__":
    rca()

x = pd.read_csv(
    "association_202101/02-vcf2pgen/pfile/BAV_TOPMED_German.chr3.pvar",
    skiprows=100,
    sep="\t",
)

x[x["chr3:19117:T:A"].str.contains("chr3:195762138")]

