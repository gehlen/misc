## Clone all GitLab repos

Clone all you github repos via GitLab API. Therefore you need an
**Personal Access Token** and your **public SSH key** deployed to GitLab.

```bash
TOKEN="<INSERT-YOUR-GITLAB-PERSONAL-ACCESS-TOKEN>"
USERNAME="<INSERT-YOUR-USERNAME>"

PREFIX="ssh_url_to_repo"
curl "https://gitlab.com/api/v4/users/$USERNAME/projects?private_token=$TOKEN" | \
grep -o "\"$PREFIX\":[^ ,]\+" | \
awk -F ':' '{printf "ssh://"; for (i=2; i<NF; i++) printf $i "/"; print $NF}' | \
xargs -L1 git clone
```