## Install admin managed conda environment

Fetch the latest conda installer from [https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh)

```bash
# Download and install in silent mode to CONDA_DIR
CONDA_DIR="/data/part1/conda"

wget "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh" -O "miniconda.sh"
sudo bash "./miniconda.sh" -b -p "$CONDA_DIR"
rm "miniconda.sh"

# Add conda group
sudo addgroup conda

# change group permissions
sudo chgrp -R conda "$CONDA_DIR"
sudo chmod 770 -R "$CONDA_DIR" #read+write
```

Next add users to this group

```
users=( "<user1>" "<user2>" "<user3>" )
for user in "${users[@]}"
do
    sudo usermod -aG conda $user
done
```

Users can initialise conda by typing

```bash
$CONDA_DIR/bin/conda init
```
