## Files & Downloads

### cURL & WebDAV for OwnCloud/Nextcloud
```bash
# Download
curl -u "<share_token>:<password>" -J -O https://example.com/public.php/webdav

# Upload
curl -u "<share_token>:<password>" -T <file_to_upload> https://example.com/public.php/webdav

# The **share_token** is located in the download link: https://example.com/index.php/s/<share_token>
# -u, --user <user:password> Server user and password
# -J, --remote-header-name Use the header-provided filename
# -O, --remote-name   Write output to a file named as the remote file
# -T, File to puT on the remote webdav.
```

### Multiple Upload to Share

```bash
TOKEN="NwC9zFEnVI8vS5v"
FILE="<FILE>"
SHARE=https://uni-bonn.sciebo.de/public.php/webdav/

find $FILE -exec curl -u ${TOKEN}:${TOKEN} -T {} $SHARE \;
```

### File operations

```bash
## Rename multiple files
# Add common prefix
for f in <files>; do mv -- "$f" "<PREFIX>.$f" ; done
# remove common prefix
rename.ul '<PREFIX>' '' <PREFIX>*

## Delete 10 days old files
sudo find <files> -type f -atime +10 -delete
# for tmp files on server:
sudo find /tmp -type f -atime +10 -delete
```