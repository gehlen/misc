### Install Docker

See also Documentation at: https://docs.docker.com/install/linux/docker-ce/ubuntu/

```bash
# Deinstall older versions
sudo apt-get -y remove docker docker-engine docker.io containerd runc

# Update package registry
sudo apt-get update

# Install dependencies
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# Download and install GPG-key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Check GPG Key: 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88
sudo apt-key fingerprint 0EBFCD88

# Add docker to repositories
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update repositories
sudo apt-get -y update

# Install docker
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

# Test Installation
sudo docker run hello-world

# Add docker group
sudo groupadd docker

# add user to docker group
sudo usermod -aG docker $USER

# Log out from shell, reconnect, then type
newgrp docker
```

> ⚠ Note: For de.NBI cloud users it might be necessary to increase mtu values (see also de.NBI [FAQ](https://cloud.denbi.de/wiki/FAQ/#i-can-not-build-docker-images-and-can-not-download-packages-from-inside-of-the-container))
> In case you use docker containers in your VM it may happen that the building process of the containers will fail or you can not download packages. The reason is that docker can not connect to the internet. The problem is caused by different mtu values for the host and the docker container. To fix the problem you have to add the correct mtu value to the following file:
>
> ```bash
> /lib/systemd/system/docker.service
> ```
>
> Add --mtu=1440 to the end of the ExecStart line in the [Service] section, like shown below:
>
> ```bash
> ExecStart=/usr/bin/docker daemon -H fd:// --mtu=1440
> ```
>
> In a last step you have to reload the daemon and restart docker:
>
> ```bash
> sudo systemctl daemon-reload
> sudo systemctl restart docker
> ```