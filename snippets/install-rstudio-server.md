## Install Rstudio Server

See also [https://rstudio.com/products/rstudio/download-server/debian-ubuntu/](https://rstudio.com/products/rstudio/download-server/debian-ubuntu/) and the administration guide at [https://docs.rstudio.com/ide/server-pro/](https://docs.rstudio.com/ide/server-pro/)

```bash
sudo apt -y install gdebi-core
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.3.959-amd64.deb -O rstudio.deb
sudo gdebi rstudio.deb # -i show no prompt
rm "rstudio.deb"
```

By default the server can be accessed on port `8787`

To change this add/change `/etc/rstudio/rserver.conf` default port.
```bash
www-port=<PORT>
```