## User management

### Add new user

```bash
# Create new user

NEWUSER=
SSHKEY=
PASSWORD=

# Create new user
sudo adduser --disabled-password --gecos "" $NEWUSER

# Add default password
echo ${NEWUSER}:${PASSWORD} | sudo chpasswd

# Change to new user
sudo su $NEWUSER

# Add SSH-Key 
cd ~
mkdir -m700 .ssh
echo "$SSHKEY" > .ssh/authorized_keys
chmod 644 .ssh/authorized_keys

```

### Add user to group

```bash
sudo usermod -aG <group> <user>
```
