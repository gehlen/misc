#!/usr/bin/env r
# Create fast manhattan plot with ggplot2 from plink association results:
suppressMessages({
  library(docopt, quietly = T)
  library(data.table, quietly = T)
  library(ggplot2, quietly = T)
})

'QQ-Plot .

Usage:
  qq.R  [-|<file>...] [options]
  qq.R  (-h | --help)
  qq.R  --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --p=P         Column with SNP P-values (or Z-statistics) [default: P]
  --ci=CI       Confidence Interval [default: 0.95]
  --plot=filename Filename of created plot [default: qq.png]   
' -> doc
opt <- docopt(doc, version = 'manhattan 0.2')

# Read from file or stdin
message("#-- Scan files")
if(!(length(opt$file) == 0)){
  dt <- lapply(opt$file, fread, 
               select = c(opt$p),
               col.names = c("P")
  )
  dt <- rbindlist(dt)
} else {
  dt <- fread("file:///dev/stdin", 
              select = c(opt$p), 
              col.names = c("P")
  )
}

# Remove NA rows
dt <- copy(dt[!is.na(P)])
dt <- dt[, P := as.numeric(P)]
ci = as.numeric(opt$ci)
n = nrow(dt)
dtp <- dt[,
          .(observed = -log10(sort(P)),
  expected = -log10(ppoints(n)),
  clower   = -log10(qbeta(p = (1 - ci) / 2, shape1 = 1:n, shape2 = n:1)),
  cupper   = -log10(qbeta(p = (1 + ci) / 2, shape1 = 1:n, shape2 = n:1)),
  P        =  sort(P)
          )]

## Calculate genomic inflation lambda
dtp[, chisq := qchisq(1-P,1)]

lambda <- dtp[, median(chisq) / qchisq(0.5, 1)]
lambda_SE <- dtp[, qchisq(0.975, 1) * (1.253 * ( sd(chisq) / sqrt( length(chisq) ) ) )]

## -- Plot
message("#-- Plotting")
log10Pe <- expression(paste('Expected -log'[10],'(',italic('P'),')'))
log10Po <- expression(paste('Observed -log'[10],'(',italic('P'),')'))

# downsample points
np <- nrow(dtp)
top = np:(np-floor(np/10))
bot = 1:floor(np/10)
rest <- max(bot):min(top)
dtp2 <- dtp[c(top, bot, sample(rest, floor(np/3)))]

# x-/y-limits
#if(max(dtp2))
xlimit <- c(0, ceiling(max(dtp2$expected) + 1))
dtp2[observed > xlimit[2], `:=` (observed = Inf)]

pl <- ggplot(dtp2) +
  geom_point(aes(expected, observed), size = .7) +
  geom_line(aes(expected, cupper), linetype = 2, color = "darkgrey") +
  geom_line(aes(expected, clower), linetype = 2, color = "darkgrey") +
  geom_abline(intercept = 0,
              slope = 1,
              color = "red") + 
  annotate(
    geom = 'text',
    label = paste("λGC =", round(lambda, 4) , '±', round(lambda_SE, 4)),
    x = Inf, # Plot in the right lower corner
    y = -Inf,
    hjust = 1.1,
    vjust = -1
  ) + 
xlab(log10Pe) +
  ylab(log10Po) +
  theme_bw() + 
  theme(
    axis.line = element_line(colour = "black"),
    panel.border = element_blank()
  ) + 
  coord_fixed(1, xlim = xlimit)
message("#-- Saving plot to ", opt$plot)
ggsave(filename = opt$plot, units = "mm", height = 148, width = 210)




