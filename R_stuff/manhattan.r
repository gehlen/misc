#!/usr/bin/env r

# Inorder to read from stdin one need littler installed.  
# Create fast manhattan plot with ggplot2 from plink association results:
suppressMessages({
  library(docopt, quietly = T)
  library(data.table, quietly = T)
  library(ggplot2, quietly = T)
  library(ggrepel, quietly = T)
  library(naturalsort, quietly = T)
  library(qqman)
})

'Manhattan.

Usage:
  manhattan.R  [-|<file>...] [options]
  manhattan.R  (-h | --help)
  manhattan.R  --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --id=ID       Column with SNP IDs [default: ID]
  --chr=CHR     Column with Chromosomes [default: #CHROM]
  --pos=POS     Column with SNP positions [default: POS]
  --p=P         Column with SNP P-values (or Z-statistics) [default: P]
  --nolog       Don\'t convert P column to -log10 scale
  --plot=filename Filename of created plot [default: manhattan.png]
  --sig=PVAL    Add line of significance [default: 5e-8]
  --sug=PVAL    Add ine of suggestive [default: 1e-5]
  --nosig       Don\'t add horizontal significance lines at -log10(1e-5) and -log10(5e-8)
  --qqman       Plot with qqman instead of ggplot2
' -> doc

opt <- docopt(doc, version = 'manhattan 0.2')

message("* Read files")
if(!(length(opt$file) == 0)){
  dt <- lapply(opt$file, fread, 
               select = c(opt$id, opt$chr, opt$pos, opt$p), 
               col.names = c("ID", "CHR", "POS", "P")
               )
  dt <- rbindlist(dt)
} else {
  dt <- fread(cmd = 'cat /dev/stdin', sep = "\t",
              select = c(opt$id, opt$chr, opt$pos, opt$p), 
              col.names = c("ID", "CHR", "POS", "P")
              )
}

message("* Create Plot")

# Remove NA rows
dt <- copy(dt[!is.na(P)])
# Change ordering based on natural sorting of chromosmes
if (opt$qqman) {
  dt[CHR == "X", CHR := 23]
  dt[, CHR := as.numeric(CHR)]
} else {
  dt[, CHR := factor(CHR, levels = naturalsort(unique(CHR)))]
}
setkey(dt, CHR, POS)

# log P column
if(opt$nolog) {
  yaxis_title = opt$p
} else {
  dt[, P := -log10(P)]
  yaxis_title = paste0("-log10(", opt$p, ")")
}

# Chromosome length
chr_info <- dt[, .(chr_length = max(POS)), by = "CHR"]
# cumulative chromosome position
chr_info[, chr_tot := cumsum(as.numeric(chr_length))-chr_length]
#left join with dt
dt <- dt[chr_info, on = "CHR"]
# cumulative SNP position
dt[, POScum := (POS + chr_tot), by = "CHR"]

# Create data.table for custom xaxis, centered chromosome number 
axis_dt <- dt[, .(center = (max(POScum) - min(POScum))/2 + unique(chr_tot)), by = "CHR"]


colors = rep(c("#263238", "#90a4ae"), ceiling(length(unique(dt[["CHR"]]))))

if (opt$qqman) {
  message("* Saving plot to ", opt$plot)
  png(
    filename = opt$plot,
    type = "cairo",
    units = "mm", height = 148, width = 210, res = 96
  )
  manhattan(dt, bp = "POS", snp = "ID", col = colors, logp = F)
  dev.off()
} else {
pl <-
  ggplot(dt, aes(x = POScum, y = P)) +
  geom_point(size = 1, aes(color = CHR)) + 
  scale_color_manual(values = colors)

pl <- pl +
  xlab("Chromosome") +
  ylab(yaxis_title) +
  scale_x_continuous(label =
                       axis_dt[["CHR"]], breaks = axis_dt[["center"]], expand = c(0.01, 0.01)) +
  scale_y_continuous(expand = c(0.01,0.01)) +     # remove space between plot area and x axis
  theme_bw() +
  theme(
    text = element_text(size = 10),
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )

# add significance lines and label snps:
if(!(opt$nosig)) {
  pl <-
    pl + geom_hline(
      yintercept = -log10(as.numeric(opt$sig)),
      linetype = "dashed",
      color = "red"
    ) +
    geom_hline(
      yintercept = -log10(as.numeric(opt$sug)),
      linetype = "dashed",
      color = "darkgrey"
    ) 
    # geom_text_repel(
    #   data = dt[P > -log10(5e-8)],
    #   aes(label = as.factor(ID)),
    #   size = 3,
    #   force = 1.5,
    #   vjust = -0.7,
    #   inherit.aes = T,
    #   color = "black"
    # )
}
message("* Saving plot to ", opt$plot)
ggsave(filename = opt$plot, plot = pl, units = "mm", height = 148, width = 210)
}
message("* Done! ✔️")