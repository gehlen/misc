---
title: Awesome Cheatcheet
author: Jan Gehlen
date: 
---


## R 

Collection of cheatsheets at [https://rstudio.com/resources/cheatsheets/](https://rstudio.com/resources/cheatsheets/)

### Most useful ones:

- [data.table](https://github.com/rstudio/cheatsheets/raw/master/datatable.pdf)
