#!/usr/bin/env python
import os
import pyqc.plot as plot
import pandas as pd

class GLM():
  """Plink2 GLM results
  """
  def __init__(self, path):
      # check if files exist, if not raise Exception
      if os.path.isfile(path) is False:
              raise Exception(f"File {path} does not exist")
      self.path = path
      self.df = self._import()
      self.lambda_gc = None
      self.lamda_gc_se = None
      # ! Implement file header Check

  def _import(self):
      print(f"Importing {self.path}.")
      df = pd.read_csv(self.path, delim_whitespace=True)
      df.rename(columns={"#CHROM": "CHR", "BP": "POS", "SNP": "ID"}, inplace=True)
      return df

  def qqplot(self, P="P", test="ADD", **kwargs):
      """QQ-plot"""
      if "TEST" in self.df:
        df = self.df.loc[self.df.TEST == test, P]
      else:
        df = self.df.P
      ax = plot.qqplot(df, **kwargs)
      return ax

  def manhattan(self, test="ADD", **kwargs):
      """Manhattan plot"""
      if "TEST" in self.df:
        df = self.df.loc[self.df.TEST == test]
      else:
        df = self.df
      ax = plot.manhattan(df, CHR="CHR", POS="POS", **kwargs)
      return ax

def meta_analyis(glms, out="Plink", plink="plink", options=["study", "weighted-z"], fields={
  "meta-analysis-snp-field": "ID",
  "meta-analysis-chr-field": "'#CHROM'",
  "meta-analysis-bp-field": "POS",
  "meta-analysis-se-field": "'LOG(OR)_SE'",
  "meta-analysis-ess-field": "OBS_CT"
  }, **flags):
  """Perform meta analysis with Plink1.9.

  Args:
      glms (list): List of GLMs
      out (str, optional): Output prefix. Defaults to "Plink".
      plink (str, optional): Path to Plink executable. Defaults to "plink".
      options (list, optional): Additional options to Plink meta-analysis. Defaults to ["study", "weighted-z"].
      fields (dict, optional): meta-anaylsis-<X>-field settings. Defaults to
      { "meta-analysis-snp-field": "ID", "meta-analysis-chr-field": "'#CHROM'",
        "meta-analysis-bp-field": "POS", "meta-analysis-se-field": "'LOG(OR)_SE'",
        "meta-analysis-ess-field": "OBS_CT" }.

  Returns:
      str: File path to meta analysis results file.
  """
  # for g in glms:
  #   if isinstance(g, GLM) is False:
  #     raise TypeError(f"must be GLM, not {type(g).__name__}")

    # Contruct commandline
  s = f"{plink} --meta-analysis "
  for g in glms:
      s += f"{g.path} "

  if options is not None:
      s += "+ "
      for o in options:
          s += f"{o} "

  for k, v in {**fields, **flags}.items():
      s += f"--{k} {v} "
  s += f"--out {out}"
  os.system(s)

  return f"{out}.meta"