import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import beta

def qqplot(pvals, ax=None, ci=.95, alpha=.2, color="b", distribution="beta", crop=12):
    '''
        Function for plotting Quantile-Quantile (QQ) plots with confidence interval (CI)
        :param data: NumPy 1D array with data
        :param ci: Confidence Interval [default: 0.95
        :param alpha: Confidence Interval Color Alpha  [default: 0.2]
        :param distribution: beta -- type of the error estimation. Most common in the literature is 'beta'.
        :param crop: Crop pvalues on -log10 scale. Default: 12
        :return: pyplot axes
        '''

    if ax is None:
        fig, ax = plt.subplots()

    # sort p values
    p_observed = np.sort(pvals)
    p_expected = np.arange(1, len(pvals) + 1)

    lobs = -(np.log10(p_observed))
    lexp = -(np.log10(p_expected / (len(pvals))))

    # Confidence interval by distribution:
    if distribution == "beta":
        err = - np.log10(beta.interval(ci, range(1, len(p_observed)+1), range(1, len(p_observed)+1)[::-1]))

    # crop pvalues
    if crop is not None:
        lobs[lobs > crop] = crop

    # Define top X percent to plot as points
    part = int(np.floor(len(pvals)*.01))

    ax.plot([0, 100], [0, 100], '--k', label="Uniform Distribution")
    ax.fill_between(lexp, err[0], err[1], alpha=alpha, color=color, label='%1.3f CI' % ci)
    ax.plot(lexp[part:], lobs[part:], "-", color=color, label='_nolegend_')
    ax.plot(lexp[:part], lobs[:part], ".", color=color, label='_nolegend_')
    ax.set_xlabel(r'Theoretical -log10($\it{P}$)')
    ax.set_ylabel(r'Experimental -log10($\it{P}$)')
    ax.set_xlim(0, lexp.max() * 1.05)
    ax.set_ylim(0, lobs.max() * 1.05)
    ax.legend(loc=4)
    return ax

def manhattan(data,ax=None, CHR="CHR", POS="BP", P="P", sig=-np.log10(5e-8), sug=-np.log10(1e-5), logP=True, Pcutoff=1e-10, figsize=(15, 5)):
    """Create Manhattan Plot

        Args:
            data (DataFrame): Data to plot
            ax (AxesSubplot): Matplotlib Axes. Defaults to None.
            CHR (str, optional): Column with Chromosome ID. Defaults to "CHR".
            POS (str, optional): Column with SNP Position. Defaults to "BP".
            P (str, optional): Column with SNP P value. Defaults to "P".
            sig (float, optional): Plot line at significance threshold. Defaults to -np.log10(5e-8).
            sug (float, optional): Plot line at suggestive threshold. Defaults to -np.log10(1e-5).
            logP (bool, optional): Logarithmize P-values. Defaults to True.
            Pcutoff ([type], optional): Crop at minimum P-values. Defaults to 1e-10.
            figsize (tuple, optional): Figure Size in Pixel * 100. Defaults to (15, 5).
        """
    # If no axes provided create one:
    if ax is None:
        fig, ax = plt.subplots()

    # calculate chromome metrics
    offset = pd.DataFrame()
    data_gr = data.groupby(CHR)
    offset['max'] = data.groupby(CHR)[POS].max()
    offset['min'] = data.groupby(CHR)[POS].min()
    offset['length'] = offset['max'] - offset['min']
    offset['off'] = offset['length'].cumsum()
    offset.off -= offset.off.iloc[0]
    # center axis label
    offset['label'] = offset['length']/2 + offset['off']

    # Calculate running SNP position
    data = data.join(offset, on=CHR)
    data['POSoff'] = data[POS] - data['min'] + data['off']

    # Crop pvalues
    data.loc[data[P] < Pcutoff, P] = Pcutoff
    # log10 pvalues
    if logP:
        data[P] = -np.log10(data[P])

    # Plot
    data_gr = data.groupby(CHR)
    for name, group in data_gr:
        ax.plot(
            group['POSoff'],
            group[P],
            marker=".",
            linestyle="",
            label=name
        )
    ax.set_xticks(offset.label.tolist())
    ax.set_xticklabels(offset.index.tolist())
    ax.set_xlabel("Chromosome")
    ax.set_ylabel("-log10($\it{P}$)")
    ax.margins(x=0.01)

    # Add significance line
    if sig and (data[P] > sig).any() :
        ax.axhline(sig, linestyle="--", color = "r")
    if sug and (data[P] > sug).any():
        ax.axhline(sug, linestyle="--", color="b")
    return ax