import os
import pandas as pd
import numpy as np
import uuid
import seaborn as sns
from scipy.stats import chi2

class PlinkFile():
    """Plink File and plink command line creator"""
    def __init__(self, bfile):
        # check if files exist, if not raise Exception
        files = [bfile + "." + ext for ext in ["bed", "bim", "fam"]]
        for f in files:
            if os.path.isfile(f) is not True:
                raise Exception("File " + f + " does not exist")
        self.bed, self.bim, self.fam = files
        self.bfile = bfile
        self.prefix = os.path.basename(bfile)

    def run_plink(self,
                  plink="plink",
                  flags={},
                  subset={"keep": None, "remove": None, "extract": None, "exclude": None},
                  outdir="./",
                  suffix=None,
                  range=False,
                  debug=False):
        """
        Construct a plink command line with flags from dict and optional subsetting with pandas DataFrames and Series.

        The subsets are first written to multiple files and then added to the plink command line via the dictionary key.
        The subsetting keys are the same as the plink command line flags: "keep", "remove", "extract", "exclude".

        :argument
            plink:  Path to Plink1.9 or Plink2 binary [default: "plink"]
            flags:  Dictionary with plink flags. See plink documentation:
            subset: (Optional) Dictionary of Pandas DataFrame(s) and/or Series
            outdir: Output directory [default: "./"]
            range:  (boolean) use plink range flag modifier for "extract" and "exclude" (enables subsetting by CHR and POS) boolean
            debug:  (boolean) Keep intermediary files [default: False]
        :returns
            exit code of plink call
        """
        # shell string s
        s = "%s " % plink

        out = os.path.join(outdir, self.prefix)
        if suffix is not None:
            out += "." + suffix
        # construct other flags/argument from dict
        for flag, argument in {
            "bfile": self.bfile,
            "out": out,
            **flags
        }.items():
            s += f"--{flag} {argument} "

        # Subset Plink file with Pandas DataFrame(s) and Series
        intfile = []  # intermediate files
        for flag, ids in subset.items():
            if ids is not None:
                # Create intermediary file
                idsfile = str(uuid.uuid4()) + ".txt"
                intfile.append(idsfile)
                # write DataFrame or Series to file,
                if type(ids) is list:
                    # Combine and keep only unique rows
                    ids = pd.concat(ids).drop_duplicates()
                ids.to_csv(idsfile, sep="\t", index=False, header=False)
                # add to command line
                s += "--{flag} {file} ".format(flag=flag, file=idsfile)
                # add range parameter to extract and exclude for position based filter
                if flag in ["extract", "exclude"] and range:
                    s += "range"

        # Call to Plink
        exitcode = os.system(s)

        # Clean Up
        if debug is False:
            # Remove intermediary files
            for file in intfile:
                os.remove(file)

        return exitcode

    def maf(self, case_control=False, pheno=None, pheno_name=None):
        """Calculate MAF and return MAF data frame

        Args:
            case_control (bool, optional): Should MAF be calculated for cases and controls seerately?. Defaults to False.
            pheno (str, optional): Plink Phenotype file. Defaults to None.
            pheno_name (str, optional): Plink Phenotype Name. Defaults to None.

        Returns:
            [DataFrame]: DataFrame with Plink MAF table.
        """
        freq = self.prefix + ".frq"

        if case_control:
            flags = {"freq": "case-control"}
            if pheno != None:
                flags["pheno"] = pheno
            if pheno_name != None:
                flags["pheno-name"] = pheno_name
            self.run_plink(flags=flags)
            freq += ".cc"
        else:
            self.run_plink(flags={"freq": ""})
        data = pd.read_csv(freq, delim_whitespace=True)
        #os.remove(freq)
        return data

    # Perform association testing for types SNPs
    def run_glm(self, pheno, pheno_name, covar=None, covar_name=None, firth="firth-fallback", force=True, *args, **flags):
        """Perform association testing by logistic regression

        Args:
            pheno (str): Plink phenotype file path.
            pheno_name (str): Phenotype name
            covar (str, optional): Plink covariate file path. Defaults to None.
            covar_name (str, optional): Covariate name(s). Please see Plink Documentaion for syntax. Defaults to None.
            firth (str, optional): Which firth method to use. Options are "no-firth", "firth-fallback, "firth", Defaults to "firth-fallback".
            force (bool, optional): Should the execution be forced, even when the resulting file already exitst. Defaults to True.


        Returns:
            str: File path to output file.
        """
        out=f"{self.prefix}"
        if covar_name is not None:
            out += f".{covar_name}"

        fl = {"glm": "", "pheno": pheno, "pheno-name": pheno_name,
            "covar-variance-standardize": "", "ci": "0.95", "out": out}
        for k, v in flags.items():
            fl[k] += f" {v}"

        if (covar_name is None) & (covar is None):
            fl["glm"] += " allow-no-covars"
        if covar_name != None:
            fl["covar-name"] = covar_name
        if covar != None:
            fl["covar"] = covar

        # output file path
        out = f"{out}.{pheno_name}.glm."
        if firth == "no-firth":
            out += "logistic"
        elif firth == "firth-fallback":
            out += "logistic.hybrid"
        elif firth == "firth":
            out += "firth"
        else:
            raise Exception("firth parameter is not valid.")

        # Check if file already exists
        if os.path.isfile(out) & (force == False):
            print(f"File {out} already exists. Use the 'force' parameter to enforce the calculation.")
        else:
            ec = self.run_plink("plink2", flags=fl)

        return out

    # Utilities
    def read_bim(self):
        """Read plink bim file to pandas DataFrame
        :returns
            Pandas DataFrame
        """
        bim = pd.read_csv(
                self.bim,
                delim_whitespace=True,
                header=None,
                names=["CHR", "SNP" ,"CM", "POS", "A1", "A2"],
                dtype={
                   "CHR": str
                }
                )
        return bim


def plink_merge(plink_list, out="Plink", flags=None):
    """Merge multiple Plink files

    Find common SNPs first then perform the merge.

    :argument
        plink_list: List of PlinkFile objects
        out: Prefix for merged data set
    :returns
        PlinkFile
    """
    # find common snps and write intermediary file
    bims = [p.read_bim()['SNP'].tolist() for p in plink_list]

    snps = set(bims[0]).intersection(*bims)
    snplist = str(uuid.uuid4()) + ".txt"
    with open(snplist, 'w') as f:
        for snp in snps:
            f.write("%s\n" % snp)

    mergelist = str(uuid.uuid4()) + ".txt"
    with open(mergelist, 'w') as f:
        for p in plink_list[1:]:
            f.write("%s\n" % p.bfile)

    bfile0 = plink_list[0]
    mflags = {
        "merge-list": mergelist,
        "extract": snplist,
        "out": out,
        "make-bed": ""
    }
    if flags is not None:
        mflags = merge_dict(flags, mflags)

    bfile0.run_plink("plink", flags=mflags)
    # remove intermediary files
    [os.remove(l) for l in [mergelist, snplist]]
    return PlinkFile(out)

def merge_dict(dict1, dict2):
    ''' Merge dictionaries and keep values of common keys in list'''
    dict3 = {**dict1, **dict2}
    for key, value in dict3.items():
        if key in dict1 and key in dict2:
            dict3[key] = [value, dict1[key]]

    return dict3

def lambda_gc(val, stat="P", ci=0.95, n_cases=None, n_controls=None):
    """Calculate Lambda GC from Statistics

    Calculates observed LambdaGC, its standard error and lambdaGC scaled to 1000 Cases and Controls.

    Args:
        val (List, pandas.Series): Values
        stat (str, optional): Choose Statistic Type. Defaults to "P".
        ci (float, optional): Calculate standard error for confidence interval. Defaults to 0.95.

    Returns:
        list: [LambdaGC, LambdaGC-SE, LambdaGC-1000]
    """
    # lambda gc from P-value
    if stat == "P":
        Z = np.sqrt(chi2.isf(val, 1))
    elif stat == "Z":
        Z = val
    elif stat == "CHISQ":
        Z = np.sqrt(val)
    else:
        raise ValueError("Possible Statistics: P,Z or CHISQ.")
    # Z to CHISQ
    CHISQ = (Z ** 2)
    # calculate lambda gc
    # formula from https://www.biostars.org/p/298847/
    lambda_gc = np.nanmedian(CHISQ) / chi2.isf(0.5,1)
    # Standard error
    alpha = (1-((1-ci)/2))
    se = chi2.isf(alpha, 1) * (1.253 * ( CHISQ.std() / np.sqrt( len(CHISQ) ) ) )

    res = [lambda_gc, se]

    # scale lambda to 1000 cases and controls
    # formula from https://www.nature.com/articles/ncomms8270.pdf?origin=ppub
    if (n_cases != None) and (n_controls != None):
        lambda_gc_1000 = 1 + ((lambda_gc - 1) * (1/n_cases + 1/n_controls) / (1/1000 + 1/1000))
        res.append(lambda_gc_1000)

    return res
