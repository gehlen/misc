# Miscellaneous scripts

This repository contains some of my scripts, maybe they are also useful to you(?)

Content:

- snippets: My snippets gathered at one place.
- postinstall: Install programs from command line with whiptail boxes.
- R_stuff: Stuff I coded in R, feel free to use any of that.
